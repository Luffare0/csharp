//Author: Gustav Svensson
//Date: 02/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
public enum ReptileSpecies
{
	Snake,
	Frog
}