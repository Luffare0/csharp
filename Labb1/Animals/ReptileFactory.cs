﻿//Author: Gustav Svensson
//Date: 02/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System.Diagnostics;
/// <summary>
/// Reptile factory.
/// </summary>
public class ReptileFactory
{
	/// <summary>
	/// Initializes a new instance of the <see cref="T:ReptileFactory"/> class.
	/// </summary>
	public ReptileFactory()
	{

	}
	/// <summary>
	/// Creates the reptile depending on which ReptileSpecie.
	/// </summary>
	/// <returns>The reptile.</returns>
	/// <param name="animalSpecie">Animal specie.</param>
	public static Reptile CreateReptile(ReptileSpecies animalSpecie)
	{
        Reptile animalObj = null;
		switch (animalSpecie)
		{
			case ReptileSpecies.Snake:
				{
                    animalObj = new Snake();
                    break;
				}
			case ReptileSpecies.Frog:
				{
					animalObj = new Frog();
                    break;
				}
            default:
                Debug.Assert(false, "To be completed");
                break;
		}
        animalObj.Category = CategoryType.Reptile;
        animalObj.Gender = GenderType.Unknown;
        return animalObj;
	}
}
