//Author: Gustav Svensson
//Date: 02/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System.Diagnostics;

/// <summary>
/// The Mammal factory class.
/// </summary>
public class MammalFactory{

	/// <summary>
	/// Initializes a new instance of the <see cref="T:MammalFactory"/> class.
	/// </summary>
  	public MammalFactory(){
    
  	}
	/// <summary>
	/// Creates a mammal depending on which MammalSpecie. 
	/// </summary>
	/// <returns>The created mammal.</returns>
	/// <param name="animalSpecie">Animal specie.</param>
	public static Mammal CreateMammal(MammalSpecies animalSpecie)
	{
        Mammal animalObj = null;
		switch (animalSpecie)
		{
			case MammalSpecies.Wolf:
				{
					animalObj = new Wolf();
                    break;
				}
			case MammalSpecies.Dog:
				{
					animalObj = new Dog();
                    break;
				}
            default:
                Debug.Assert(false, "To be completed");
                break;
        }
        animalObj.Category = CategoryType.Mammal;
        animalObj.Gender = GenderType.Unknown;
        return animalObj;
	}
}
