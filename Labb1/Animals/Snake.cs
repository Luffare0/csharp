//Author: Gustav Svensson
//Date: 02/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using Labb1.Animals;
/// <summary>
/// The snake class. Represents a snake.
/// Inherits from Reptile. 
/// </summary>
public class Snake : Reptile {
    private int length;
    private FoodSchedule foodschedule;
    private EaterType eatertype;
    /// <summary>
    /// Initializes a new instance of the <see cref="T:Snake"/> class.
    /// </summary>
    public Snake()      
    {
        eatertype = EaterType.Carnivore;
        foodschedule = new Labb1.Animals.FoodSchedule();
        foodschedule.AddFoodScheduleItem("Morning: 2 mice");
        foodschedule.AddFoodScheduleItem("Lunch: 2 mice");
        foodschedule.AddFoodScheduleItem("Evening: 2 mice");
    }
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Snake"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="name">Name.</param>
	/// <param name="age">Age.</param>
	/// <param name="gender">Gender.</param>
	/// <param name="category">Category.</param>
	/// <param name="length">Length.</param>
    public Snake(int id, string name, int age, GenderType gender,CategoryType category, int length) : base(id, name, age, gender, category){
        this.length = length;
        eatertype = EaterType.Carnivore;
    }
	/// <summary>
	/// Gets or sets the length.
	/// </summary>
	/// <value>The length.</value>
    public int Length {
        get { return length; }
        set { length = value; }
    }
	/// <summary>
	/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Snake"/>.
	/// </summary>
	/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Snake"/>.</returns>
    public override string ToString()
    {
        return base.ToString() + "Is of length " + length + ". ";
    }

    public override string GetSpecies()
    {
        return "snake";
    }

    public override FoodSchedule GetFoodSchedule()
    {
        return foodschedule;
    }

    public override EaterType GetEaterType()
    {
        return eatertype;
    }
}
