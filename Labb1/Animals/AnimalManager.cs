//Author: Gustav Svensson
//Date: 02/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Windows.Forms;

/// <summary>
/// The Animal manager class.
/// </summary>
public class AnimalManager{
  	private List<Animal> m_animalList;
  	private int currentId;

	/// <summary>
	/// Initializes a new instance of the <see cref="T:AnimalManager"/> class. Initilizes the m_animalList. 
	/// which holds all animals. 
	/// </summary>
 	public AnimalManager(){
    m_animalList = new List<Animal>();
    currentId = 0;  
  	}
	/// <summary>
	/// Adds the animal to the m_animalList. 
	/// </summary>
	/// <param name="animalObj">Animal object.</param>
    internal void addAnimal(Animal animalObj)
    {
		animalObj.Id = currentId++;
        m_animalList.Add(animalObj);
    }

	/// <summary>
	/// Gets the animal at the given index.
	/// </summary>
	/// <returns>The animal at index.</returns>
	/// <param name="index">Index.</param>
	internal Animal GetAnimalAt(int index)
	{
		return m_animalList[index];
	}

	/// <summary>
	/// Gets or sets the list count.
	/// </summary>
	/// <value>The list count.</value>
	public int ListCount
	{
		get { return m_animalList.Count; }
		set
		{
		}
	}

	/// <summary>
	/// Gets a list of all animals in string format. 
	/// </summary>
	/// <returns>The all animals to string.</returns>
	internal string[] GetAllAnimalsToString()

	{
		string[] m_animalListString = new string[ListCount];
		for (int i = 0; i < ListCount; i++)
		{
			m_animalListString[i] = m_animalList[i].ToString();
		}
		return m_animalListString;
	}
}
