﻿
namespace MainForm
{
    partial class Animals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.listCategory = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listAnimals = new System.Windows.Forms.ListBox();
            this.listAllAnimals = new System.Windows.Forms.ListBox();
            this.AddAnimalButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.NameAgeGender = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listGender = new System.Windows.Forms.ListBox();
            this.NameField = new System.Windows.Forms.TextBox();
            this.AgeField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Specifications = new System.Windows.Forms.GroupBox();
            this.LengthField = new System.Windows.Forms.TextBox();
            this.BreedField = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TailField = new System.Windows.Forms.TextBox();
            this.TeethField = new System.Windows.Forms.TextBox();
            this.TailLength = new System.Windows.Forms.Label();
            this.NoOfTeeth = new System.Windows.Forms.Label();
            this.listAnimalsAndCategory = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SpecificationsColumn = new System.Windows.Forms.Label();
            this.NameColumn = new System.Windows.Forms.Label();
            this.CategoryColumn = new System.Windows.Forms.Label();
            this.GenderColumn = new System.Windows.Forms.Label();
            this.AgeColumn = new System.Windows.Forms.Label();
            this.IDColumn = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.feedingBox = new System.Windows.Forms.TextBox();
            this.eaterBox = new System.Windows.Forms.TextBox();
            this.eaterTypeLabel = new System.Windows.Forms.Label();
            this.NameAgeGender.SuspendLayout();
            this.Specifications.SuspendLayout();
            this.listAnimalsAndCategory.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(464, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Category";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // listCategory
            // 
            this.listCategory.FormattingEnabled = true;
            this.listCategory.Location = new System.Drawing.Point(143, 15);
            this.listCategory.Name = "listCategory";
            this.listCategory.Size = new System.Drawing.Size(120, 225);
            this.listCategory.TabIndex = 7;
            this.listCategory.SelectedIndexChanged += new System.EventHandler(this.selectedCategory);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, -1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Animal Object";
            // 
            // listAnimals
            // 
            this.listAnimals.FormattingEnabled = true;
            this.listAnimals.Location = new System.Drawing.Point(17, 15);
            this.listAnimals.Name = "listAnimals";
            this.listAnimals.Size = new System.Drawing.Size(120, 225);
            this.listAnimals.TabIndex = 9;
            this.listAnimals.SelectedIndexChanged += new System.EventHandler(this.selectAnimal);
            // 
            // listAllAnimals
            // 
            this.listAllAnimals.FormattingEnabled = true;
            this.listAllAnimals.HorizontalScrollbar = true;
            this.listAllAnimals.Location = new System.Drawing.Point(18, 45);
            this.listAllAnimals.Name = "listAllAnimals";
            this.listAllAnimals.Size = new System.Drawing.Size(584, 199);
            this.listAllAnimals.TabIndex = 10;
            this.listAllAnimals.SelectedIndexChanged += new System.EventHandler(this.listAllAnimals_SelectedIndexChanged);
            // 
            // AddAnimalButton
            // 
            this.AddAnimalButton.Location = new System.Drawing.Point(143, 246);
            this.AddAnimalButton.Name = "AddAnimalButton";
            this.AddAnimalButton.Size = new System.Drawing.Size(120, 31);
            this.AddAnimalButton.TabIndex = 12;
            this.AddAnimalButton.Text = "Add this new animal";
            this.AddAnimalButton.UseVisualStyleBackColor = true;
            this.AddAnimalButton.Click += new System.EventHandler(this.addAnimal);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 13;
            // 
            // NameAgeGender
            // 
            this.NameAgeGender.Controls.Add(this.label3);
            this.NameAgeGender.Controls.Add(this.label1);
            this.NameAgeGender.Controls.Add(this.listGender);
            this.NameAgeGender.Controls.Add(this.NameField);
            this.NameAgeGender.Controls.Add(this.AgeField);
            this.NameAgeGender.Controls.Add(this.label2);
            this.NameAgeGender.Location = new System.Drawing.Point(3, 4);
            this.NameAgeGender.Name = "NameAgeGender";
            this.NameAgeGender.Size = new System.Drawing.Size(171, 141);
            this.NameAgeGender.TabIndex = 17;
            this.NameAgeGender.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Gender";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // listGender
            // 
            this.listGender.FormattingEnabled = true;
            this.listGender.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Unknown"});
            this.listGender.Location = new System.Drawing.Point(54, 65);
            this.listGender.Name = "listGender";
            this.listGender.Size = new System.Drawing.Size(93, 56);
            this.listGender.TabIndex = 5;
            // 
            // NameField
            // 
            this.NameField.Location = new System.Drawing.Point(47, 13);
            this.NameField.Name = "NameField";
            this.NameField.Size = new System.Drawing.Size(100, 20);
            this.NameField.TabIndex = 1;
            // 
            // AgeField
            // 
            this.AgeField.Location = new System.Drawing.Point(100, 39);
            this.AgeField.Name = "AgeField";
            this.AgeField.Size = new System.Drawing.Size(47, 20);
            this.AgeField.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Age";
            // 
            // Specifications
            // 
            this.Specifications.Controls.Add(this.LengthField);
            this.Specifications.Controls.Add(this.BreedField);
            this.Specifications.Controls.Add(this.label8);
            this.Specifications.Controls.Add(this.label6);
            this.Specifications.Controls.Add(this.TailField);
            this.Specifications.Controls.Add(this.TeethField);
            this.Specifications.Controls.Add(this.TailLength);
            this.Specifications.Controls.Add(this.NoOfTeeth);
            this.Specifications.Location = new System.Drawing.Point(3, 159);
            this.Specifications.Name = "Specifications";
            this.Specifications.Size = new System.Drawing.Size(315, 133);
            this.Specifications.TabIndex = 18;
            this.Specifications.TabStop = false;
            this.Specifications.Text = "Specifications";
            // 
            // LengthField
            // 
            this.LengthField.Location = new System.Drawing.Point(260, 91);
            this.LengthField.Name = "LengthField";
            this.LengthField.Size = new System.Drawing.Size(49, 20);
            this.LengthField.TabIndex = 7;
            // 
            // BreedField
            // 
            this.BreedField.Location = new System.Drawing.Point(260, 65);
            this.BreedField.Name = "BreedField";
            this.BreedField.Size = new System.Drawing.Size(49, 20);
            this.BreedField.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Length";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Breed";
            // 
            // TailField
            // 
            this.TailField.Location = new System.Drawing.Point(260, 39);
            this.TailField.Name = "TailField";
            this.TailField.Size = new System.Drawing.Size(49, 20);
            this.TailField.TabIndex = 3;
            // 
            // TeethField
            // 
            this.TeethField.Location = new System.Drawing.Point(260, 13);
            this.TeethField.Name = "TeethField";
            this.TeethField.Size = new System.Drawing.Size(49, 20);
            this.TeethField.TabIndex = 2;
            // 
            // TailLength
            // 
            this.TailLength.AutoSize = true;
            this.TailLength.Location = new System.Drawing.Point(16, 46);
            this.TailLength.Name = "TailLength";
            this.TailLength.Size = new System.Drawing.Size(56, 13);
            this.TailLength.TabIndex = 1;
            this.TailLength.Text = "Tail length";
            // 
            // NoOfTeeth
            // 
            this.NoOfTeeth.AutoSize = true;
            this.NoOfTeeth.Location = new System.Drawing.Point(16, 20);
            this.NoOfTeeth.Name = "NoOfTeeth";
            this.NoOfTeeth.Size = new System.Drawing.Size(63, 13);
            this.NoOfTeeth.TabIndex = 0;
            this.NoOfTeeth.Text = "No. of teeth";
            // 
            // listAnimalsAndCategory
            // 
            this.listAnimalsAndCategory.Controls.Add(this.listAnimals);
            this.listAnimalsAndCategory.Controls.Add(this.listCategory);
            this.listAnimalsAndCategory.Controls.Add(this.AddAnimalButton);
            this.listAnimalsAndCategory.Controls.Add(this.label5);
            this.listAnimalsAndCategory.Location = new System.Drawing.Point(324, 4);
            this.listAnimalsAndCategory.Name = "listAnimalsAndCategory";
            this.listAnimalsAndCategory.Size = new System.Drawing.Size(279, 288);
            this.listAnimalsAndCategory.TabIndex = 19;
            this.listAnimalsAndCategory.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SpecificationsColumn);
            this.groupBox1.Controls.Add(this.NameColumn);
            this.groupBox1.Controls.Add(this.CategoryColumn);
            this.groupBox1.Controls.Add(this.GenderColumn);
            this.groupBox1.Controls.Add(this.AgeColumn);
            this.groupBox1.Controls.Add(this.IDColumn);
            this.groupBox1.Controls.Add(this.listAllAnimals);
            this.groupBox1.Location = new System.Drawing.Point(3, 298);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(600, 260);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List of registered animals";
            // 
            // SpecificationsColumn
            // 
            this.SpecificationsColumn.AutoSize = true;
            this.SpecificationsColumn.Location = new System.Drawing.Point(461, 29);
            this.SpecificationsColumn.Name = "SpecificationsColumn";
            this.SpecificationsColumn.Size = new System.Drawing.Size(73, 13);
            this.SpecificationsColumn.TabIndex = 17;
            this.SpecificationsColumn.Text = "Specifications";
            // 
            // NameColumn
            // 
            this.NameColumn.AutoSize = true;
            this.NameColumn.Location = new System.Drawing.Point(75, 29);
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.Size = new System.Drawing.Size(35, 13);
            this.NameColumn.TabIndex = 16;
            this.NameColumn.Text = "Name";
            // 
            // CategoryColumn
            // 
            this.CategoryColumn.AutoSize = true;
            this.CategoryColumn.Location = new System.Drawing.Point(309, 29);
            this.CategoryColumn.Name = "CategoryColumn";
            this.CategoryColumn.Size = new System.Drawing.Size(49, 13);
            this.CategoryColumn.TabIndex = 15;
            this.CategoryColumn.Text = "Category";
            // 
            // GenderColumn
            // 
            this.GenderColumn.AutoSize = true;
            this.GenderColumn.Location = new System.Drawing.Point(233, 29);
            this.GenderColumn.Name = "GenderColumn";
            this.GenderColumn.Size = new System.Drawing.Size(42, 13);
            this.GenderColumn.TabIndex = 14;
            this.GenderColumn.Text = "Gender";
            // 
            // AgeColumn
            // 
            this.AgeColumn.AutoSize = true;
            this.AgeColumn.Location = new System.Drawing.Point(171, 29);
            this.AgeColumn.Name = "AgeColumn";
            this.AgeColumn.Size = new System.Drawing.Size(26, 13);
            this.AgeColumn.TabIndex = 13;
            this.AgeColumn.Text = "Age";
            // 
            // IDColumn
            // 
            this.IDColumn.AutoSize = true;
            this.IDColumn.Location = new System.Drawing.Point(23, 29);
            this.IDColumn.Name = "IDColumn";
            this.IDColumn.Size = new System.Drawing.Size(18, 13);
            this.IDColumn.TabIndex = 11;
            this.IDColumn.Text = "ID";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.feedingBox);
            this.groupBox2.Controls.Add(this.eaterBox);
            this.groupBox2.Controls.Add(this.eaterTypeLabel);
            this.groupBox2.Location = new System.Drawing.Point(611, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(354, 357);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Feeding schedule";
            // 
            // feedingBox
            // 
            this.feedingBox.Location = new System.Drawing.Point(9, 39);
            this.feedingBox.Multiline = true;
            this.feedingBox.Name = "feedingBox";
            this.feedingBox.Size = new System.Drawing.Size(339, 312);
            this.feedingBox.TabIndex = 4;
            // 
            // eaterBox
            // 
            this.eaterBox.Location = new System.Drawing.Point(110, 13);
            this.eaterBox.Name = "eaterBox";
            this.eaterBox.Size = new System.Drawing.Size(238, 20);
            this.eaterBox.TabIndex = 2;
            // 
            // eaterTypeLabel
            // 
            this.eaterTypeLabel.AutoSize = true;
            this.eaterTypeLabel.Location = new System.Drawing.Point(6, 16);
            this.eaterTypeLabel.Name = "eaterTypeLabel";
            this.eaterTypeLabel.Size = new System.Drawing.Size(59, 13);
            this.eaterTypeLabel.TabIndex = 1;
            this.eaterTypeLabel.Text = "Eater Type";
            // 
            // Animals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 566);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Specifications);
            this.Controls.Add(this.NameAgeGender);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listAnimalsAndCategory);
            this.Name = "Animals";
            this.Text = "Animals";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.NameAgeGender.ResumeLayout(false);
            this.NameAgeGender.PerformLayout();
            this.Specifications.ResumeLayout(false);
            this.Specifications.PerformLayout();
            this.listAnimalsAndCategory.ResumeLayout(false);
            this.listAnimalsAndCategory.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listCategory;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listAnimals;
        private System.Windows.Forms.ListBox listAllAnimals;
        private System.Windows.Forms.Button AddAnimalButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox NameAgeGender;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listGender;
        private System.Windows.Forms.TextBox NameField;
        private System.Windows.Forms.TextBox AgeField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox Specifications;
        private System.Windows.Forms.GroupBox listAnimalsAndCategory;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TailField;
        private System.Windows.Forms.TextBox TeethField;
        private System.Windows.Forms.Label TailLength;
        private System.Windows.Forms.Label NoOfTeeth;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox LengthField;
        private System.Windows.Forms.TextBox BreedField;
        private System.Windows.Forms.Label AgeColumn;
        private System.Windows.Forms.Label IDColumn;
        private System.Windows.Forms.Label GenderColumn;
        private System.Windows.Forms.Label SpecificationsColumn;
        private System.Windows.Forms.Label NameColumn;
        private System.Windows.Forms.Label CategoryColumn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label eaterTypeLabel;
        private System.Windows.Forms.TextBox feedingBox;
        private System.Windows.Forms.TextBox eaterBox;
    }
}

