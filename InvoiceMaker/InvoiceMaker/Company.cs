﻿namespace InvoiceMaker
{
    internal class Company
    {
        string name;
        private string address;
        private string zipcode;
        private string city;
        private string country;
        private string phoneNbr;
        private string Url;

        public Company()
        {

        }

        public Company(string name, string address, string zipcode, string city, string country, string phoneNbr, string Url)
        {
            this.name = name;
            this.address = address;
            this.zipcode = zipcode;
            this.city = city;
            this.country = country;
            this.phoneNbr = phoneNbr;
            this.Url = Url;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string Zipcode
        {
            get { return zipcode; }
            set { zipcode = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        public string PhoneNbr
        {
            get { return phoneNbr; }
            set { phoneNbr = value; }
        }

        public string URL
        {
            get { return Url; }
            set { Url = value; }
        }
    }
}