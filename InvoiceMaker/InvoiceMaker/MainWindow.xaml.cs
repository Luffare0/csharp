﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InvoiceMaker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Invoice invoice;
        public MainWindow()
        {
            InitializeComponent();
            invoiceNbrBox.IsEnabled = false;
            clientCompany.IsEnabled = false;
            clientAddress1.IsEnabled = false;
            clientCountry.IsEnabled = false;
            clientCity.IsEnabled = false;
            clientName.IsEnabled = false;
            totalAmount.IsEnabled = false;
        }

        private async void OpenInvoice_Click(object sender, RoutedEventArgs e)
        {
            readInvoice();
            UpdateGUI();
        }

        private void UpdateGUI()
        {
            if (invoice != null) { 
                invoiceNbrBox.Text = invoice.InvoiceNbr.ToString();
                invoiceDateBox.Text = invoice.InvoiceDate.ToShortDateString();
                dueDateBox.Text = invoice.DueDate.ToShortDateString();
                clientCompany.Text = invoice.SendToCompany.Name;
                clientAddress1.Text = invoice.SendToCompany.Address;
                clientCountry.Text = invoice.SendToCompany.Country;
                clientCity.Text = invoice.SendToCompany.City;
                double totalAllItems = 0.0;
                itemsList.Items.Clear();
                for (int i = 0; i < invoice.Items.Count; i++)
                {
                    Item item = invoice.Items[i];
                    double totalTax = (double)item.Quantity * item.UnitPrice * (item.Tax / 100.0);
                    double total = totalTax + (item.UnitPrice * item.Quantity);
                    itemsList.Items.Add(new { Item = i + 1, Description = item.Description, Quantity = item.Quantity, UnitPrice = item.UnitPrice, Tax = item.Tax, TotalTax = totalTax, Total = total });
                    totalAllItems += total;
                }
                companyAddress.Content = invoice.SenderCompany.Address;
                companyPhone.Content = invoice.SenderCompany.PhoneNbr;
                cityLabel.Content = invoice.SenderCompany.City;
                homePage.Content = invoice.SenderCompany.URL;
                double discount = 0.0;
                try
                {
                    discount = Double.Parse(discountBox.Text.Replace(',', '.'), CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Not a valid discount");
                    return;
                }
                if (discount > totalAllItems)
                {
                    MessageBox.Show("Not a valid discount");
                    return;
                }
                else
                {
                    totalAllItems -= discount;
                }
                totalAmount.Text = totalAllItems.ToString();
            }
        }

        private void readInvoice()
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString();
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Filter = "Text File (*.txt)|*.txt";
                openFileDialog1.FilterIndex = 2;
                Nullable<bool> result = openFileDialog1.ShowDialog();
                this.invoice = new Invoice();
                if (result == true)
                {
                    Debug.Print("hello");
                    StreamReader sr = new StreamReader(openFileDialog1.FileName);
                    int invoiceNbr = 0;
                    string line = sr.ReadLine();
                    Int32.TryParse(line, out invoiceNbr);
                    invoice.InvoiceNbr = invoiceNbr;
                    line = sr.ReadLine();
                    DateTime invoiceDate = DateTime.Parse(line);
                    invoice.InvoiceDate = invoiceDate;
                    line = sr.ReadLine();
                    DateTime dueDate = DateTime.Parse(line);
                    invoice.DueDate = dueDate;
                    Company sendToCompany = new Company();
                    line = sr.ReadLine();
                    sendToCompany.Name = line;
                    line = sr.ReadLine();
                    clientName.Text = line;
                    line = sr.ReadLine();
                    sendToCompany.Address = line;
                    line = sr.ReadLine();
                    sendToCompany.Zipcode = line;
                    line = sr.ReadLine();
                    sendToCompany.City = line;
                    line = sr.ReadLine();
                    sendToCompany.Country = line;
                    invoice.SendToCompany = sendToCompany;
                    line = sr.ReadLine();
                    int nbrItems = 0;
                    Int32.TryParse(line, out nbrItems);
                    for (int j = 0; j < nbrItems; j++)
                    {
                        Item item = new Item();
                        line = sr.ReadLine();
                        string description = line;
                        item.Description = description;
                        line = sr.ReadLine();
                        int quantity = 0;
                        Int32.TryParse(line, out quantity);
                        item.Quantity = quantity;
                        line = sr.ReadLine();
                        double unitPrice = Double.Parse(line.Replace(',', '.'), CultureInfo.InvariantCulture);
                        item.UnitPrice = unitPrice;
                        line = sr.ReadLine();
                        double tax = Double.Parse(line.Replace(',', '.'), CultureInfo.InvariantCulture);
                        item.Tax = tax;
                        invoice.AddItem(item);
                    }
                    Company senderCompany = new Company();
                    line = sr.ReadLine();
                    senderCompany.Name = line;
                    line = sr.ReadLine();
                    senderCompany.Address = line;
                    line = sr.ReadLine();
                    senderCompany.Zipcode = line;
                    line = sr.ReadLine();
                    senderCompany.City = line;
                    line = sr.ReadLine();
                    senderCompany.Country = line;
                    line = sr.ReadLine();
                    senderCompany.PhoneNbr = line;
                    line = sr.ReadLine();
                    senderCompany.URL = line;
                    invoice.SenderCompany = senderCompany;
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not read invoice");
            }
        }

        private void invoiceDateBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                DateTime invoiceDate;
                try
                {
                    invoiceDate = DateTime.Parse(invoiceDateBox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Not a valid date");
                    return;
                }
                invoice.InvoiceDate = invoiceDate;
            }
        }

        private void dueDateBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DateTime dueDate;
                try
                {
                    dueDate = DateTime.Parse(dueDateBox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Not a valid date");
                    return;
                }
                invoice.DueDate = dueDate;
            }
        }

        private void discountBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                UpdateGUI();
        }
    }
}
