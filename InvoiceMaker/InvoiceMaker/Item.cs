﻿namespace InvoiceMaker
{
    internal class Item
    {
        private string description;
        private double unitPrice;
        private double tax;
        private int quantity;

        public Item()
        {

        }

        public Item(string description, double unitPrice, double tax, int quantity)
        {
            this.quantity = quantity;
            this.description = description;
            this.unitPrice = unitPrice;
            this.tax = tax;
        }

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public double UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public double Tax
        {
            get { return tax; }
            set { tax = value; }
        }

    }
}