﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceMaker
{
    class Invoice
    {
        private int invoiceNbr;
        private DateTime invoiceDate;
        private DateTime dueDate;
        private Company sendToCompany;
        private List<Item> items;
        private Company senderCompany;

        public Invoice()
        {
            items = new List<Item>();
        }

        public Invoice(int invoiceNbr, DateTime invoiceDate, DateTime dueDate)
        {
            this.invoiceNbr = invoiceNbr;
            this.invoiceDate = invoiceDate;
            this.dueDate = dueDate;
            items = new List<Item>();
        }

        public Invoice(int invoiceNbr, DateTime invoiceDate, DateTime dueDate, Company sendToCompany, List<Item> items, Company senderCompany)
        {
            this.invoiceNbr = invoiceNbr;
            this.invoiceDate = invoiceDate;
            this.dueDate = dueDate;
            this.sendToCompany = sendToCompany;
            this.items = items;
            this.senderCompany = senderCompany;
        }

        public int InvoiceNbr
        {
            get { return invoiceNbr;  }
            set { invoiceNbr = value; }
        }

        public DateTime InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public DateTime DueDate
        {
            get { return dueDate;  }
            set { dueDate = value; }
        }

        public Company SendToCompany
        {
            get { return sendToCompany;  }
            set { sendToCompany = value; }
        }

        public Company SenderCompany
        {
            get { return senderCompany;  }
            set { senderCompany = value; }
        }

        public List<Item> Items
        {
            get { return items; }
            set { items = value; }
        }

        internal void AddItem(Item item)
        {
            items.Add(item);
        }
    }
}
