﻿//Author: Gustav Svensson
//Date: 16/5 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ControlTower
{
    /// <summary>
    /// Interaction logic for FlightWindow.xaml
    /// </summary>
    public partial class FlightWindow : Window
    {
        private string flight;
        private int route;
        private string status;
        public event EventHandler<LandEventInfo> LandEvent;
        public event EventHandler<TakeOffEventInfo> TakeOffEvent;
        public event EventHandler<ChangeRouteEventInfo> ChangeRouteEvent;


        /// <summary>
        /// Initializes the window.
        /// </summary>
        public FlightWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the window with parameters. 
        /// </summary>
        /// <param name="flight"></param>
        /// <param name="status"></param>
        /// <param name="route"></param>
        public FlightWindow(string flight, string status, int route)
        {
            InitializeComponent();
            this.flight = flight;
            this.status = status;
            this.route = route;
            landButton.IsEnabled = false;
            string[] airline = flight.Split(null);
            var uriSource = new Uri(@"pack://application:,,,/Images/airplane.jpg");
            bool found = false;
            if (airline[0] == "SAS")
            {
                found = true;
                uriSource = new Uri(@"pack://application:,,,/Images/Scandinavian_Airlines_logo.svg.png");
            }else if(airline[0] == "DLH")
            {
                found = true;
                uriSource = new Uri(@"pack://application:,,,/Images/Lufthansa-Logo-fly-880x660.png");
            }else if(airline[0] == "UAL")
            {
                found = true;
                uriSource = new Uri(@"pack://application:,,,/Images/united-airlines-logo_1974-1993.png");
            }
            Debug.Print(found.ToString());
            BitmapImage logo = new BitmapImage();
            airLineLogo.Source = new BitmapImage(uriSource);
        }

        /// <summary>
        /// Is called when the route is changed in the GUI. Sends an changeroutevent to the subscriber. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int[] routes = { 5, 10, 15, 20, 25, 30 };
            int index = comboBox.SelectedIndex;
            ChangeRouteEventInfo changeRouteEventInfo = new ControlTower.ChangeRouteEventInfo(flight, DateTime.Now, routes[index]);
            OnChangeRouteEvent(changeRouteEventInfo);    
        }

        /// <summary>
        /// Is called when the land button is clicked. Send an land event to the subscriber.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void landButton_Click(object sender, RoutedEventArgs e)
        {
            LandEventInfo landEventInfo = new LandEventInfo(flight, DateTime.Now);
            OnLandEvent(landEventInfo);
            this.Close();
        }

        /// <summary>
        /// Is called when the start button is clicked. Send an start event to the subscriber. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            startButton.IsEnabled = false;
            landButton.IsEnabled = true;
            TakeOffEventInfo takeOffEventInfo = new TakeOffEventInfo(flight, DateTime.Now);
            OnTakeOffEvent(takeOffEventInfo);
        }

        /// <summary>
        /// Sends land event to subscriber. 
        /// </summary>
        /// <param name="e"></param>
        private void OnLandEvent(LandEventInfo e)
        {
            if(LandEvent != null)
            {
                LandEvent(this, e);
            }
        }

        /// <summary>
        /// Sends take off event to subscriber. 
        /// </summary>
        /// <param name="e"></param>
        private void OnTakeOffEvent(TakeOffEventInfo e)
        {
            if(TakeOffEvent != null)
            {
                TakeOffEvent(this, e);
            }
        }

        /// <summary>
        /// Sends change route event to subscriber. 
        /// </summary>
        /// <param name="e"></param>
        private void OnChangeRouteEvent(ChangeRouteEventInfo e)
        {
            if(ChangeRouteEvent != null)
            {
                ChangeRouteEvent(this, e);
            }
        }
    }
}
