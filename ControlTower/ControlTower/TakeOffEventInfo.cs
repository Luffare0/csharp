﻿//Author: Gustav Svensson
//Date: 16/5 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTower
{
    /// <summary>
    /// This class represents the information for the take off event. 
    /// </summary>
    public class TakeOffEventInfo : EventArgs
    {
        private string flight;
        private DateTime time;

        /// <summary>
        /// The constructor for TakeOffEventInfo.
        /// </summary>
        /// <param name="flight"></param>
        /// <param name="time"></param>
        public TakeOffEventInfo(string flight, DateTime time)
        {
            this.flight = flight;
            this.time = time;
        }

        /// <summary>
        /// gets or sets the flight.
        /// </summary>
        public string Flight
        {
            get { return flight; }
            set { flight = value; }
        }

        //gets or sets the time. 
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}
