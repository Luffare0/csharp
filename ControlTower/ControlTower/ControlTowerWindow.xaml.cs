﻿//Author: Gustav Svensson
//Date: 16/5 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ControlTower
{
    /// <summary>
    /// Interaction logic for ControlTowerWindow.xaml
    /// </summary>
    public partial class ControlTowerWindow : Window
    {
        /// <summary>
        /// Initializes the controltowerwindow.
        /// </summary>
        public ControlTowerWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This method is called when the buttopn send to runway is clicked. Makes the control tower window a subscriber to the publisher(the flight). 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            string flight = flightCodeBox.Text;
            if (flight != "")
            {
                string status = "Sent to runway";
                int route = 0;
                flightList.Items.Add(new {Flight = flight, Status = status, Time = DateTime.Now });
                FlightWindow flightwindow = new ControlTower.FlightWindow(flight, status, route);          
                flightwindow.Show();
                flightwindow.TakeOffEvent += OnTakeOffSent;
                flightwindow.LandEvent += OnLandSent;
                flightwindow.ChangeRouteEvent += OnChangeRouteEvent;
            }
        }
        /// <summary>
        /// Updates the GUI when an take off event has happened. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTakeOffSent(object sender, TakeOffEventInfo e)
        {
            string flight = e.Flight;
            DateTime time = e.Time;
            flightList.Items.Add(new { Flight = flight, Status = "Started", Time = time });
        }

        /// <summary>
        /// Updates teh GUI when an Land event has happened. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLandSent(object sender, LandEventInfo e)
        {
            string flight = e.Flight;
            DateTime time = e.Time;
            flightList.Items.Add(new { Flight = flight, Status = "Landed", Time = time });
        }

        /// <summary>
        /// Updates the GUI when an Change Route event has happened. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnChangeRouteEvent(object sender, ChangeRouteEventInfo e)
        {
            string flight = e.Flight;
            DateTime time = e.Time;
            int route = e.Route;
            flightList.Items.Add(new { Flight = flight, Status = "Now heading " + route + " deg", Time = time });
        }
    }
}
