﻿//Author: Gustav Svensson
//Date: 16/5 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlTower
{
    /// <summary>
    /// This class represents the information for the land event.
    /// </summary>
    public class LandEventInfo : EventArgs
    {
        private string flight;
        private DateTime time;

        /// <summary>
        /// The constructor for LandEventInfo.
        /// </summary>
        /// <param name="flight"></param>
        /// <param name="time"></param>
        public LandEventInfo(string flight, DateTime time)
        {
            this.flight = flight;
            this.time = time;
        }

        /// <summary>
        /// Gets or sets the flight.
        /// </summary>
        public string Flight
        {
            get { return flight; }
            set { flight = value; }
        }

        /// <summary>
        /// Gets or sets the time. 
        /// </summary>
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}

