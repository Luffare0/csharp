﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CaclulatorDelegateCS.MathLib
{
    public delegate double CalculateHandler(double value1, double value2);

    public class CalcManager
    {
        // declared but not created
        private CalculateHandler calcDelegate;

        /// <summary>
        /// Method that performs the required calculation by delegating the job to the 
        /// delegate CalculateHandler
        /// </summary>
        /// 
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public double DoCalculate(double value1, double value2, CalcOperators.Operators operation)
        {

            switch (operation)
            {
                case CalcOperators.Operators.Add:
                    calcDelegate = new CalculateHandler(Calculator.Add);
                    break;
                case CalcOperators.Operators.Substract:
                    calcDelegate = new CalculateHandler(Calculator.Subtract);
                    break;
                case CalcOperators.Operators.Multiply:
                    calcDelegate = new CalculateHandler(Calculator.Multiply);
                    break;
                case CalcOperators.Operators.Divide:
                    calcDelegate = new CalculateHandler(Calculator.Divide);
                    break;
                case CalcOperators.Operators.Power:
                    calcDelegate = new CalculateHandler(Calculator.Power);
                    break;
            }

            //which function? don't know until runtime
            return calcDelegate(value1, value2);
        }

        //return a delegate
        public CalculateHandler GetMethod(CalcOperators.Operators operation)
        {
            CalculateHandler calcMethod = null;

            switch (operation)
            {
                case CalcOperators.Operators.Add:
                    calcMethod = Calculator.Add;
                    break;
                case CalcOperators.Operators.Substract:
                    calcMethod = Calculator.Subtract;
                    break;
                case CalcOperators.Operators.Multiply:
                    calcMethod = Calculator.Multiply;
                    break;
                case CalcOperators.Operators.Divide:
                    calcMethod = Calculator.Divide;
                    break;
                case CalcOperators.Operators.Power:
                    calcMethod = Calculator.Power;
                    break;
            }

            return calcMethod;
        }

    }
}
