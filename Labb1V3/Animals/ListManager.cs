﻿//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Labb1.Animals
{
    /// <summary>
    /// List manager.
    /// </summary>
	public class ListManager<T> : IListManager<T>
    {
        private List<T> m_list;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Labb1.Animals.ListManager`1"/> class.
        /// </summary>
		public ListManager()
        {
            m_list = new List<T>();
        }


		/// <summary>
		/// Returns the number of items in the m_list collection.
		/// </summary>
		public int Count
        {
            get
            {
                return m_list.Count;
            }
        }


		/// <summary>
		/// Add an object to the collection m_list. 
		/// </summary>
		/// <param name="aType">A type</param>
		/// <returns>True if successful, false otherwise</returns>
        public virtual bool Add(T aType)
        {
            m_list.Add(aType);
            return true;
        }

		/// <summary>
		/// Change an object in the collection m_list at the given index.
		/// </summary>
		/// <param name="aType">A type</param>
		/// <param name="anindex">An index</param>
		/// <returns>True if successful, false otherwise</returns>
		public bool ChangeAt(T aType, int anindex)
        {
            m_list[anindex] = aType;
            return true;
        }

		/// <summary>
		/// Check if the index is within the boundaries of the collection m_list.
		/// </summary>
		/// <param name="index"></param>
		/// <returns>True if successful, false otherwise</returns>
		public bool CheckIndex(int index)
        {
            return index < m_list.Count;
        }

		/// <summary>
		/// Deletes all of the items in the collection m_list.
		/// </summary>
		public void DeleteAll()
        {
            m_list.Clear();
        }

		/// <summary>
		/// Remove an object from the collection m_list at
		/// a given position. 
		/// </summary>
		/// <param name="anIndex">Index to the object that is to be removed.</param>
		/// <returns>True if successful, false otherwise.</returns>
		public bool DeleteAt(int anIndex)
        {
            m_list.RemoveAt(anIndex);
            return true;
        }

		/// <summary>
		/// Gets the element T in the collection m_list at the given index.
		/// </summary>
		/// <param name="anIndex">An index.</param>
		/// <returns></returns>
		public T GetAt(int anIndex)
        {
            return m_list[anIndex];
        }

		/// <summary>
		/// Returns an array of the string representations of 
		/// the items in the m_list collection.
		/// </summary>
		/// <returns>A string array</returns>
		public string[] ToStringArray()
        {
            string[] stringarray = new string[m_list.Count];
            for (int i = 0; i < m_list.Count; i++){
                stringarray[i] = m_list[i].ToString();
            }
            return stringarray;
        }

		/// <summary>
		/// Returns a list of the string representations of 
		/// the items in the m_list collection.
		/// </summary>
		/// <returns>A string list.</returns>
		public List<string> ToStringList()
        {
            List<String> stringlist = new List<String>();
            for (int i = 0; i < m_list.Count; i++)
            {
                stringlist[i] = m_list[i].ToString();
            }
            return stringlist;
        }
    }
}
