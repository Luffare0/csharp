//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using Labb1.Animals;
/// <summary>
/// The Mammal class. Represents a mammal.
/// Inherits from Animal. 
/// </summary>
public abstract class Mammal : Animal {
    private int teeth;
    public Mammal()
    {

    }

    public Mammal(int id, string name, int age, GenderType gender, CategoryType category, int teeth) : base(id, name, age, gender, category){
        this.teeth = teeth;
    }

    public int Teeth
    {
        get { return teeth; }
        set
        {
            teeth = value;
        }
    }

    public override string ToString()
    {
        return base.ToString() + "Has " + teeth + " teeth. " ;
    }
}
