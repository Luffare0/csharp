//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using Labb1.Animals;
/// <summary>
/// The dog class, inherits from Mammal.
/// </summary>
public class Dog : Mammal {
    private string breed;
    private FoodSchedule foodschedule;
    private EaterType eatertype;
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Dog"/> class.
	/// </summary>
    public Dog()
    {
        eatertype = EaterType.Carnivore;
        foodschedule = new Labb1.Animals.FoodSchedule();
        foodschedule.AddFoodScheduleItem("Morning: bones and one glas milk!");
        foodschedule.AddFoodScheduleItem("Lunch: dog food and water.");
        foodschedule.AddFoodScheduleItem("3PM: Sweets.");
        foodschedule.AddFoodScheduleItem("Evenings: Rests from the kitchen.");

    }
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Dog"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="name">Name.</param>
	/// <param name="age">Age.</param>
	/// <param name="gender">Gender.</param>
	/// <param name="category">Category.</param>
	/// <param name="teeth">Teeth.</param>
	/// <param name="breed">Breed.</param>
    public Dog(int id, string name, int age, GenderType gender, CategoryType category, int teeth, string breed) : base(id, name, age, gender, category, teeth){
        this.breed = breed;
        eatertype = EaterType.Carnivore;
        foodschedule = new Labb1.Animals.FoodSchedule();
        foodschedule.AddFoodScheduleItem("Morning: bones and one glas milk!");
        foodschedule.AddFoodScheduleItem("Lunch: dog food and water.");
        foodschedule.AddFoodScheduleItem("3PM: Sweets.");
        foodschedule.AddFoodScheduleItem("Evenings: Rests from the kitchen.");
    }
	/// <summary>
	/// Gets or sets the breed.
	/// </summary>
	/// <value>The breed.</value>
    public string Breed {

        get { return breed;  }
        set
        {
            breed = value;
        }
    }
	/// <summary>
	/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Dog"/>.
	/// </summary>
	/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Dog"/>.</returns>
    public override string ToString()
    {
        return base.ToString() + "Is of breed " + breed + ".";
    }

    public override string GetSpecies()
    {
        return "dog";
    }

    public override FoodSchedule GetFoodSchedule()
    {
        return foodschedule;
    }

    public override EaterType GetEaterType()
    {
        return eatertype;
    }
}
