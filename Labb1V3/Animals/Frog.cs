//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using Labb1.Animals;
/// <summary>
/// The frog class. Represents a frog. 
/// Inherits from Reptile. 
/// </summary>
public class Frog : Reptile {
    private FoodSchedule foodschedule;
    private EaterType eatertype;
    /// <summary>
    /// Initializes a new instance of the <see cref="T:Frog"/> class.
    /// </summary>
	public Frog()
    {
        eatertype = EaterType.Carnivore;
        foodschedule = new FoodSchedule();
        foodschedule.DescribeNoFeedingRequired();
    }
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Frog"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="name">Name.</param>
	/// <param name="age">Age.</param>
	/// <param name="gender">Gender.</param>
	/// <param name="category">Category.</param>
    public Frog(int id, string name, int age, GenderType gender, CategoryType category) : base(id, name, age, gender, category){
        eatertype = EaterType.Carnivore;
        foodschedule = new FoodSchedule();
        foodschedule.DescribeNoFeedingRequired();
    }
	/// <summary>
	/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Frog"/>.
	/// </summary>
	/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Frog"/>.</returns>
    public override string ToString()
    {
        return base.ToString();
    }

    public override string GetSpecies()
    {
        return "frog";
    }

    public override FoodSchedule GetFoodSchedule()
    {
        return foodschedule;
    }

    public override EaterType GetEaterType()
    {
        return eatertype;
    }
}
