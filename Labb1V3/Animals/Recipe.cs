﻿//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Labb1.Animals
{
    
    /// <summary>
    /// The recipe class. Reprsents a recipe.
    /// </summary>
	public class Recipe
    {
        private ListManager<string> m_ingredients;
        private string name;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Animal"/> class.
		/// </summary>
		public Recipe()
        {
            m_ingredients = new ListManager<string>();
        }

        /// <summary>
        /// Gets or sets the ingredients.
        /// </summary>
        /// <value>The ingredients.</value>
		public ListManager<string> Ingredients{
            get { return m_ingredients; }
            set { m_ingredients = value; }
        }

		/// <summary>
		/// Gets the count.
		/// </summary>
		/// <value>The count.</value>
		public int Count { 
			get { return m_ingredients.Count; }
		}

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
		public string Name {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Labb1.Animals.Recipe"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Labb1.Animals.Recipe"/>.</returns>
		public override string ToString()
        {
            string recipe =  "";
            int size = m_ingredients.Count;
			recipe += name + ", ";
            for(int i = 0; i  < size; i++)
            {
				if (i == size - 1)
				{
					recipe += m_ingredients.GetAt(i);
				}
				else
				{
					recipe += m_ingredients.GetAt(i) + ", ";
				}
					
            }
            return recipe;
        }
    }
}
