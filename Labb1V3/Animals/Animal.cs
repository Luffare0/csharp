//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using Labb1.Animals;
/// <summary>
/// The Animal class. Represents an animal. 
/// </summary>
public abstract class Animal : IAnimal
{
    private int id;
    private string name;
    private int age;
    private GenderType gender;
    private CategoryType category;

	/// <summary>
	/// Initializes a new instance of the <see cref="T:Animal"/> class.
	/// </summary>
    public Animal()
    {

    }
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Animal"/> class. Creates an 
	/// animal with id, name, age, gender and category.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="name">Name.</param>
	/// <param name="age">Age.</param>
	/// <param name="gender">Gender.</param>
	/// <param name="category">Category.</param>
    public Animal(int id, string name, int age, GenderType gender, CategoryType category)
    {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
	/// <summary>
	/// Gets or sets the name.
	/// </summary>
	/// <value>The name.</value>
    public string Name
    {
        get { return name; }
        set
        {
            name = value;
        }
    }
	/// <summary>
	/// Gets or sets the age.
	/// </summary>
	/// <value>The age.</value>
    public int Age
    {
        get { return age; }
        set
        {
            age = value;
        }
    }
	/// <summary>
	/// Gets or sets the gender.
	/// </summary>
	/// <value>The gender.</value>
    public GenderType Gender
    {
        get { return gender; }
        set
        {
            gender = value;
        }
    }
	/// <summary>
	/// Gets or sets the category.
	/// </summary>
	/// <value>The category.</value>
    public CategoryType Category
    {
        get { return category; }
        set
        {
            category = value;
        }
    }
	/// <summary>
	/// Gets or sets the id.
	/// </summary>
	/// <value>The identifier.</value>
    public int Id
    {
        get { return id; }
        set
        {
            id = value;
        }
    }
	/// <summary>
	/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Animal"/>.
	/// </summary>
	/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Animal"/>.</returns>
    public override string ToString()
    {
        return "     " + id + "                 " + name + "                 " + age + "                 " + gender.ToString() +  "                 " + category.ToString() + "                        ";
    }

    public abstract EaterType GetEaterType();

    public abstract string GetSpecies();

    public abstract FoodSchedule GetFoodSchedule();
}
