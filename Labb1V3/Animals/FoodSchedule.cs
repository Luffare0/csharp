﻿//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Labb1.Animals
{
    /// <summary>
    /// The FoodSchedule class. Represents a foodschedule for a animal. 
    /// </summary>
    public class FoodSchedule
    {
        private List<String> foodDescriptionList;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:FoodSchedule"/> class.
        /// </summary>
        public FoodSchedule()
        {
            foodDescriptionList = new List<String>();
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:FoodSchedule"/> class with a existing list.
        /// </summary>
        /// <param name="foodDescriptionList"></param>
        public FoodSchedule(List<String> foodDescriptionList)
        {
            this.foodDescriptionList = foodDescriptionList;
        }

        /// <summary>
        /// Returns the number of items in the foodDescriptionList.
        /// </summary>
        public int Count
        {
            get { return foodDescriptionList.Count; }
            set { }
        }

        /// <summary>
        /// Adds an item to the foodDescriptionList.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool AddFoodScheduleItem(string item)
        {
            foodDescriptionList.Add(item);
            return true;
        }

        /// <summary>
        /// Replaces an item in the list on the specified index with the given item.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool ChangeFoodScheduleItem(string item, int index)
        {
            if (!ValidateIndex(index))
            {
                return false;
            }else
            {
                foodDescriptionList[index] = item;
                return true;
            }
        }

        /// <summary>
        /// Deletes an item in the list on the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool DeleteFoodScheduleItem(int index)
        {
            if (!ValidateIndex(index))
            {
                return false;
            }
            else
            {
                foodDescriptionList.RemoveAt(index);
                return true;
            }
        }

        /// <summary>
        /// Returns a string that describes no feeding required for a animal.
        /// </summary>
        /// <returns></returns>
        public string DescribeNoFeedingRequired()
        {
            return "No feeding required";
        }

        /// <summary>
        /// Returns the foodschedule item at the given index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetFoodSchedule(int index)
        {
            if (!ValidateIndex(index))
            {
                return null;
            }else
            {
                return foodDescriptionList[index];
            }
        }

        /// <summary>
        /// Returns a string representation of the foodschedule. 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string schedule = "";
            int size = foodDescriptionList.Count;
            schedule += "To be fed " + size + " times as follows" + Environment.NewLine;
            for (int i = 0; i < foodDescriptionList.Count; i++)
            {
                schedule += "(" + i + ")" + foodDescriptionList[i] + Environment.NewLine;
            }
            return schedule;
        }

        /// <summary>
        /// Validates if the index is within the foodDescriptionList.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool ValidateIndex(int index)
        {
            return foodDescriptionList.Count > index;
        }
    }
}
