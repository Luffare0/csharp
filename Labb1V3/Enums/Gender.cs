//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
public enum GenderType{
  Female,
  Male,
  Unknown
}
