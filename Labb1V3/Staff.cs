﻿//Author: Gustav Svensson
//Date: 20/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using Labb1.Animals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Labb1
{
    /// <summary>
    /// A class representing a staff member, using a listmanager.
    /// </summary>
    public class Staff
    {
        private string name;
        private IListManager<string> m_qualifications;

        /// <summary>
        /// Initalizes a new instance of the <see cref="T:Staff"/> class.
        /// </summary>
        public Staff()
        {
            m_qualifications = new ListManager<string>();
        }

        /// <summary>
        /// Gets or sets the ListManager containing a staff members qualifications.
        /// </summary>
        public ListManager<string> Qualifications
        {
            get { return (ListManager <string>) m_qualifications; }
            set { m_qualifications = value; }
        }

        /// <summary>
        /// Gets or sets the name for a staff member. 
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        
        /// <summary>
        /// Gets the number of items in the m_qualifications manager. 
        /// </summary>
        public int Count
        {
            get { return m_qualifications.Count; }
        }

        /// <summary>
        /// Returns a string representation of a staff member. 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string recipe = "";
            int size = m_qualifications.Count;
            recipe += name + ", ";
            for (int i = 0; i < size; i++)
            {
                if (i == size - 1)
                {
                    recipe += m_qualifications.GetAt(i);
                }
                else
                {
                    recipe += m_qualifications.GetAt(i) + ", ";
                }

            }
            return recipe;
        }
    }
}
