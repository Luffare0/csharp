﻿
    partial class Animals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.listCategory = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listAnimals = new System.Windows.Forms.ListBox();
            this.AddAnimalButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.NameAgeGender = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listGender = new System.Windows.Forms.ListBox();
            this.NameField = new System.Windows.Forms.TextBox();
            this.AgeField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Specifications = new System.Windows.Forms.GroupBox();
            this.LengthField = new System.Windows.Forms.TextBox();
            this.BreedField = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TailField = new System.Windows.Forms.TextBox();
            this.TeethField = new System.Windows.Forms.TextBox();
            this.TailLength = new System.Windows.Forms.Label();
            this.NoOfTeeth = new System.Windows.Forms.Label();
            this.listAnimalsAndCategory = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Delete = new System.Windows.Forms.Button();
            this.eaterBox = new System.Windows.Forms.TextBox();
            this.eaterTypeLabel = new System.Windows.Forms.Label();
            this.feedingBox = new System.Windows.Forms.TextBox();
            this.addFoodButton = new System.Windows.Forms.Button();
            this.foodList = new System.Windows.Forms.ListBox();
            this.addStaffButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mnuFile = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileXML = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileXMLExport = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.NameAgeGender.SuspendLayout();
            this.Specifications.SuspendLayout();
            this.listAnimalsAndCategory.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.mnuFile.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(140, -1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Category";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // listCategory
            // 
            this.listCategory.FormattingEnabled = true;
            this.listCategory.Location = new System.Drawing.Point(143, 15);
            this.listCategory.Name = "listCategory";
            this.listCategory.Size = new System.Drawing.Size(120, 225);
            this.listCategory.TabIndex = 7;
            this.listCategory.SelectedIndexChanged += new System.EventHandler(this.selectedCategory);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, -1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Animal Object";
            // 
            // listAnimals
            // 
            this.listAnimals.FormattingEnabled = true;
            this.listAnimals.Location = new System.Drawing.Point(17, 15);
            this.listAnimals.Name = "listAnimals";
            this.listAnimals.Size = new System.Drawing.Size(120, 225);
            this.listAnimals.TabIndex = 9;
            this.listAnimals.SelectedIndexChanged += new System.EventHandler(this.selectAnimal);
            // 
            // AddAnimalButton
            // 
            this.AddAnimalButton.Location = new System.Drawing.Point(144, 264);
            this.AddAnimalButton.Name = "AddAnimalButton";
            this.AddAnimalButton.Size = new System.Drawing.Size(120, 31);
            this.AddAnimalButton.TabIndex = 12;
            this.AddAnimalButton.Text = "Add this new animal";
            this.AddAnimalButton.UseVisualStyleBackColor = true;
            this.AddAnimalButton.Click += new System.EventHandler(this.addAnimal);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 13;
            // 
            // NameAgeGender
            // 
            this.NameAgeGender.Controls.Add(this.label3);
            this.NameAgeGender.Controls.Add(this.label1);
            this.NameAgeGender.Controls.Add(this.listGender);
            this.NameAgeGender.Controls.Add(this.NameField);
            this.NameAgeGender.Controls.Add(this.AgeField);
            this.NameAgeGender.Controls.Add(this.label2);
            this.NameAgeGender.Location = new System.Drawing.Point(3, 28);
            this.NameAgeGender.Name = "NameAgeGender";
            this.NameAgeGender.Size = new System.Drawing.Size(171, 125);
            this.NameAgeGender.TabIndex = 17;
            this.NameAgeGender.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Gender";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // listGender
            // 
            this.listGender.FormattingEnabled = true;
            this.listGender.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Unknown"});
            this.listGender.Location = new System.Drawing.Point(54, 63);
            this.listGender.Name = "listGender";
            this.listGender.Size = new System.Drawing.Size(93, 56);
            this.listGender.TabIndex = 5;
            // 
            // NameField
            // 
            this.NameField.Location = new System.Drawing.Point(47, 13);
            this.NameField.Name = "NameField";
            this.NameField.Size = new System.Drawing.Size(100, 20);
            this.NameField.TabIndex = 1;
            // 
            // AgeField
            // 
            this.AgeField.Location = new System.Drawing.Point(100, 39);
            this.AgeField.Name = "AgeField";
            this.AgeField.Size = new System.Drawing.Size(47, 20);
            this.AgeField.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Age";
            // 
            // Specifications
            // 
            this.Specifications.Controls.Add(this.LengthField);
            this.Specifications.Controls.Add(this.BreedField);
            this.Specifications.Controls.Add(this.label8);
            this.Specifications.Controls.Add(this.label6);
            this.Specifications.Controls.Add(this.TailField);
            this.Specifications.Controls.Add(this.TeethField);
            this.Specifications.Controls.Add(this.TailLength);
            this.Specifications.Controls.Add(this.NoOfTeeth);
            this.Specifications.Location = new System.Drawing.Point(3, 159);
            this.Specifications.Name = "Specifications";
            this.Specifications.Size = new System.Drawing.Size(315, 176);
            this.Specifications.TabIndex = 18;
            this.Specifications.TabStop = false;
            this.Specifications.Text = "Specifications";
            // 
            // LengthField
            // 
            this.LengthField.Location = new System.Drawing.Point(260, 91);
            this.LengthField.Name = "LengthField";
            this.LengthField.Size = new System.Drawing.Size(49, 20);
            this.LengthField.TabIndex = 7;
            // 
            // BreedField
            // 
            this.BreedField.Location = new System.Drawing.Point(260, 65);
            this.BreedField.Name = "BreedField";
            this.BreedField.Size = new System.Drawing.Size(49, 20);
            this.BreedField.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Length";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Breed";
            // 
            // TailField
            // 
            this.TailField.Location = new System.Drawing.Point(260, 39);
            this.TailField.Name = "TailField";
            this.TailField.Size = new System.Drawing.Size(49, 20);
            this.TailField.TabIndex = 3;
            // 
            // TeethField
            // 
            this.TeethField.Location = new System.Drawing.Point(260, 13);
            this.TeethField.Name = "TeethField";
            this.TeethField.Size = new System.Drawing.Size(49, 20);
            this.TeethField.TabIndex = 2;
            // 
            // TailLength
            // 
            this.TailLength.AutoSize = true;
            this.TailLength.Location = new System.Drawing.Point(16, 46);
            this.TailLength.Name = "TailLength";
            this.TailLength.Size = new System.Drawing.Size(56, 13);
            this.TailLength.TabIndex = 1;
            this.TailLength.Text = "Tail length";
            // 
            // NoOfTeeth
            // 
            this.NoOfTeeth.AutoSize = true;
            this.NoOfTeeth.Location = new System.Drawing.Point(12, 20);
            this.NoOfTeeth.Name = "NoOfTeeth";
            this.NoOfTeeth.Size = new System.Drawing.Size(63, 13);
            this.NoOfTeeth.TabIndex = 0;
            this.NoOfTeeth.Text = "No. of teeth";
            // 
            // listAnimalsAndCategory
            // 
            this.listAnimalsAndCategory.Controls.Add(this.listAnimals);
            this.listAnimalsAndCategory.Controls.Add(this.listCategory);
            this.listAnimalsAndCategory.Controls.Add(this.AddAnimalButton);
            this.listAnimalsAndCategory.Controls.Add(this.label5);
            this.listAnimalsAndCategory.Controls.Add(this.label4);
            this.listAnimalsAndCategory.Location = new System.Drawing.Point(324, 28);
            this.listAnimalsAndCategory.Name = "listAnimalsAndCategory";
            this.listAnimalsAndCategory.Size = new System.Drawing.Size(279, 307);
            this.listAnimalsAndCategory.TabIndex = 19;
            this.listAnimalsAndCategory.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Controls.Add(this.Delete);
            this.groupBox1.Controls.Add(this.eaterBox);
            this.groupBox1.Controls.Add(this.feedingBox);
            this.groupBox1.Controls.Add(this.eaterTypeLabel);
            this.groupBox1.Location = new System.Drawing.Point(4, 341);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(962, 281);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List of registered animals and their feeding schedules";
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(18, 48);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(662, 198);
            this.listView1.TabIndex = 24;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(18, 252);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 23);
            this.Delete.TabIndex = 23;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // eaterBox
            // 
            this.eaterBox.Location = new System.Drawing.Point(695, 22);
            this.eaterBox.Name = "eaterBox";
            this.eaterBox.Size = new System.Drawing.Size(261, 20);
            this.eaterBox.TabIndex = 2;
            // 
            // eaterTypeLabel
            // 
            this.eaterTypeLabel.AutoSize = true;
            this.eaterTypeLabel.Location = new System.Drawing.Point(630, 25);
            this.eaterTypeLabel.Name = "eaterTypeLabel";
            this.eaterTypeLabel.Size = new System.Drawing.Size(59, 13);
            this.eaterTypeLabel.TabIndex = 1;
            this.eaterTypeLabel.Text = "Eater Type";
            // 
            // feedingBox
            // 
            this.feedingBox.Location = new System.Drawing.Point(695, 50);
            this.feedingBox.Multiline = true;
            this.feedingBox.Name = "feedingBox";
            this.feedingBox.Size = new System.Drawing.Size(260, 196);
            this.feedingBox.TabIndex = 4;
            this.feedingBox.Tag = "";
            // 
            // addFoodButton
            // 
            this.addFoodButton.Location = new System.Drawing.Point(6, 19);
            this.addFoodButton.Name = "addFoodButton";
            this.addFoodButton.Size = new System.Drawing.Size(75, 23);
            this.addFoodButton.TabIndex = 24;
            this.addFoodButton.Text = "Add Food";
            this.addFoodButton.UseVisualStyleBackColor = true;
            this.addFoodButton.Click += new System.EventHandler(this.addFoodButton_Click);
            // 
            // foodList
            // 
            this.foodList.FormattingEnabled = true;
            this.foodList.Location = new System.Drawing.Point(97, 18);
            this.foodList.Name = "foodList";
            this.foodList.Size = new System.Drawing.Size(251, 277);
            this.foodList.TabIndex = 25;
            this.foodList.DoubleClick += new System.EventHandler(this.foodList_DoubleClick_1);
            // 
            // addStaffButton
            // 
            this.addStaffButton.Location = new System.Drawing.Point(6, 48);
            this.addStaffButton.Name = "addStaffButton";
            this.addStaffButton.Size = new System.Drawing.Size(75, 23);
            this.addStaffButton.TabIndex = 26;
            this.addStaffButton.Text = "Add staff";
            this.addStaffButton.UseVisualStyleBackColor = true;
            this.addStaffButton.Click += new System.EventHandler(this.addStaffButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(6, 77);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 27;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.deleteButton);
            this.groupBox2.Controls.Add(this.addStaffButton);
            this.groupBox2.Controls.Add(this.foodList);
            this.groupBox2.Controls.Add(this.addFoodButton);
            this.groupBox2.Location = new System.Drawing.Point(611, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(354, 307);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Food and staff information";
            // 
            // mnuFile
            // 
            this.mnuFile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mnuFile.Location = new System.Drawing.Point(0, 0);
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(978, 24);
            this.mnuFile.TabIndex = 24;
            this.mnuFile.Text = "mnuFile";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileNew,
            this.mnuFileOpen,
            this.mnuFileSave,
            this.mnuFileSaveAs,
            this.mnuFileXML,
            this.mnuFileExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // mnuFileNew
            // 
            this.mnuFileNew.Name = "mnuFileNew";
            this.mnuFileNew.Size = new System.Drawing.Size(124, 22);
            this.mnuFileNew.Text = "New";
            this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
            // 
            // mnuFileOpen
            // 
            this.mnuFileOpen.Name = "mnuFileOpen";
            this.mnuFileOpen.Size = new System.Drawing.Size(124, 22);
            this.mnuFileOpen.Text = "Open ...";
            this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Size = new System.Drawing.Size(124, 22);
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveAs
            // 
            this.mnuFileSaveAs.Name = "mnuFileSaveAs";
            this.mnuFileSaveAs.Size = new System.Drawing.Size(124, 22);
            this.mnuFileSaveAs.Text = "Save as ...";
            this.mnuFileSaveAs.Click += new System.EventHandler(this.mnuFileSaveAs_Click);
            // 
            // mnuFileXML
            // 
            this.mnuFileXML.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileXMLExport});
            this.mnuFileXML.Name = "mnuFileXML";
            this.mnuFileXML.Size = new System.Drawing.Size(124, 22);
            this.mnuFileXML.Text = "XML";
            // 
            // mnuFileXMLExport
            // 
            this.mnuFileXMLExport.Name = "mnuFileXMLExport";
            this.mnuFileXMLExport.Size = new System.Drawing.Size(169, 22);
            this.mnuFileXMLExport.Text = "Export to XML File";
            this.mnuFileXMLExport.Click += new System.EventHandler(this.mnuFileXMLExport_Click);
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Size = new System.Drawing.Size(124, 22);
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // Animals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 625);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Specifications);
            this.Controls.Add(this.NameAgeGender);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.listAnimalsAndCategory);
            this.Controls.Add(this.mnuFile);
            this.Name = "Animals";
            this.Text = "Animals";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.NameAgeGender.ResumeLayout(false);
            this.NameAgeGender.PerformLayout();
            this.Specifications.ResumeLayout(false);
            this.Specifications.PerformLayout();
            this.listAnimalsAndCategory.ResumeLayout(false);
            this.listAnimalsAndCategory.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.mnuFile.ResumeLayout(false);
            this.mnuFile.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listCategory;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listAnimals;
        private System.Windows.Forms.Button AddAnimalButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox NameAgeGender;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listGender;
        private System.Windows.Forms.TextBox NameField;
        private System.Windows.Forms.TextBox AgeField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox Specifications;
        private System.Windows.Forms.GroupBox listAnimalsAndCategory;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TailField;
        private System.Windows.Forms.TextBox TeethField;
        private System.Windows.Forms.Label TailLength;
        private System.Windows.Forms.Label NoOfTeeth;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox LengthField;
        private System.Windows.Forms.TextBox BreedField;
        private System.Windows.Forms.Label eaterTypeLabel;
        private System.Windows.Forms.TextBox feedingBox;
        private System.Windows.Forms.TextBox eaterBox;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button addFoodButton;
        private System.Windows.Forms.ListBox foodList;
        private System.Windows.Forms.Button addStaffButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MenuStrip mnuFile;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuFileNew;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSave;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSaveAs;
        private System.Windows.Forms.ToolStripMenuItem mnuFileXML;
        private System.Windows.Forms.ToolStripMenuItem mnuFileXMLExport;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExit;
    private System.Windows.Forms.ListView listView1;
}


