﻿//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Labb1;

    /// <summary>
    /// Animals.
    /// </summary>
    public partial class Animals : Form
    {
        private AnimalManager m_manager;
        private RecipeManager m_foodManager;
        private IListManager<Staff> m_staffManager;
        bool hasSaved;
        string fileName;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:MainForm.Animals"/> class.
        /// </summary>
		public Animals()
        {
            InitializeComponent();

        }
        /// <summary>
        /// Loads the GUI. Adds Mammal and Reptile to the CategoryList.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void Form1_Load(object sender, EventArgs e)
        {
            listCategory.Items.Add(CategoryType.Mammal);
            listCategory.Items.Add(CategoryType.Reptile);
            eaterBox.ReadOnly = true;
            feedingBox.ReadOnly = true;
            m_manager = new AnimalManager();
            m_foodManager = new RecipeManager();
            m_staffManager = new ListManager<Staff>();
            hasSaved = false;
            fileName = "";
            listView1.BeginUpdate();
            listView1.View = View.Details;
            listView1.FullRowSelect = true;
            listView1.Columns.Add("Species", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Id", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Name", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Age", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Gender", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Category", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Specifications", -2, HorizontalAlignment.Left);
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            listView1.EndUpdate();
        }
        
        /// <summary>
        /// Adds the animal to the animalm_manager.
        /// </summary>
        private void AddAnimal()
        {
            Animal animalObj = null;
            object category = listCategory.SelectedItem;
            if (category == null)
            {
                System.Windows.Forms.MessageBox.Show("Must select a category, animal was not added");
                return;
            }
            CategoryType animalCategory = (CategoryType)Enum.Parse(typeof(CategoryType), category.ToString());

            int age;
            object species = listAnimals.SelectedItem;
            if (species == null)
            {
                System.Windows.Forms.MessageBox.Show("Must select a species, animal was not added");
                return;
            }
            switch (animalCategory)
            {
                case CategoryType.Mammal:
                    {
                        MammalSpecies animalSpecie = (MammalSpecies)Enum.Parse(typeof(MammalSpecies), species.ToString());

                        animalObj = MammalFactory.CreateMammal(animalSpecie);
                        int teeth;
                        if (int.TryParse(TeethField.Text, out teeth))
                        {
                            if (teeth < 0)
                            {
                                System.Windows.Forms.MessageBox.Show("Number of teeth cant be negative, animal was not added");
                                return;
                            }
                            ((Mammal)animalObj).Teeth = teeth;
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("No of teeth must be a number, animal was not added");
                            return;
                        }
                        if (animalObj is Dog)
                        {
                            string breed = BreedField.Text;
                            if (breed.Equals(""))
                            {
                                System.Windows.Forms.MessageBox.Show("Must enter a breed, animal was not added");
                            }
                            ((Dog)animalObj).Breed = BreedField.Text;
                        }

                        if (animalObj is Wolf)
                        {
                            int tailLength;
                            if (int.TryParse(TailField.Text, out tailLength))
                            {
                                if (tailLength < 0)
                                {
                                    System.Windows.Forms.MessageBox.Show("The tail length cant be negative, animal was not added");
                                    return;
                                }
                                ((Wolf)animalObj).TailLength = tailLength;
                            }
                            else
                            {
                                System.Windows.Forms.MessageBox.Show("Tail length must be a number, animal was not added");
                                return;
                            }
                        }
                        break;
                    }

                case CategoryType.Reptile:
                    {
                        ReptileSpecies animalSpecie = (ReptileSpecies)Enum.Parse(typeof(ReptileSpecies), species.ToString());
                        animalObj = ReptileFactory.CreateReptile(animalSpecie);
                        if (animalObj is Snake)
                        {
                            int length;
                            if (int.TryParse(LengthField.Text, out length))
                            {
                                if (length < 0)
                                {
                                    System.Windows.Forms.MessageBox.Show("Length cant be negative, animal was not added");
                                    return;
                                }
                                ((Snake)animalObj).Length = length;
                            }
                            else
                            {
                                System.Windows.Forms.MessageBox.Show("Length must be a number, animal was not added");
                                return;
                            }
                        }
                        break;
                    }
            }

            if (int.TryParse(AgeField.Text, out age))
            {
                if (age < 0)
                {
                    System.Windows.Forms.MessageBox.Show("Age cant be negative, animal was not added");
                    return;
                }
                animalObj.Age = age;
            }
            else
            {
                //error
                System.Windows.Forms.MessageBox.Show("Age must be a number, animal was not added");
                return;
            }
            string name = NameField.Text;
            object gender = listGender.SelectedItem;
            if (gender == null)
            {
                System.Windows.Forms.MessageBox.Show("Must select a gender, animal was not added");
                return;
            }
            if (name.Equals(""))
            {
                System.Windows.Forms.MessageBox.Show("Must enter a name, animal was not added");
                return;
            }
            else
            {
                animalObj.Name = name;
                animalObj.Gender = (GenderType)Enum.Parse(typeof(GenderType), gender.ToString());
                m_manager.Add(animalObj);
            }
            updateGUI();
        }

        /// <summary>
        /// Updates the GUI. Specifically the list with all animals.
        /// </summary>
        private void updateGUI()
        {
            listView1.Items.Clear();
            char[] splitter  = { ';'};
            for (int i = 0; i < m_manager.Count; i++)
            {
                string animal = m_manager.GetAt(i).ToString();
                string[] split = animal.Split(splitter);
                ListViewItem row = new ListViewItem();
                row.Text = m_manager.GetAt(i).GetSpecies();
                System.Diagnostics.Debug.Print(animal);
                row.SubItems.Add(split[0]);
                row.SubItems.Add(split[1]);
                row.SubItems.Add(split[2]);
                row.SubItems.Add(split[3]);
                row.SubItems.Add(split[4]);
                row.SubItems.Add(split[5]);
                listView1.Items.Add(row);
            }
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            listView1.Refresh();
            foodList.Items.Clear();
            for (int i = 0; i < m_foodManager.Count; i++)
            {
                foodList.Items.Add(m_foodManager.GetAt(i));
            }
            for (int i = 0; i < m_staffManager.Count; i++)
            {
                foodList.Items.Add(m_staffManager.GetAt(i));
            }
            //foodList.Items.Add(m_foodManager);
            //foodList.Items.AddRange(m_foodManager.ToStringArray());
            //foodList.Items.AddRange(m_staffManager.ToStringArray());
        }
        /// <summary>
        /// Called when pushing the add animal button in the GUI. Calls AddAnimal.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>

        private void addAnimal(object sender, EventArgs e)
        {
            AddAnimal();
        }
        /// <summary>
        /// Updates the GUI depending on which category is chosen in the GUI.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void selectedCategory(object sender, EventArgs e)
        {
            listAnimals.Items.Clear();
            CategoryType animalCategory = (CategoryType)Enum.Parse(typeof(CategoryType), listCategory.SelectedItem.ToString());
            switch (animalCategory)
            {
                case CategoryType.Mammal:
                    {
                        LengthField.Clear();
                        LengthField.Enabled = false;
                        TeethField.Enabled = true;
                        TailField.Enabled = true;
                        listAnimals.Items.Add(MammalSpecies.Dog);
                        listAnimals.Items.Add(MammalSpecies.Wolf);
                        break;
                    }
                case CategoryType.Reptile:
                    {
                        TeethField.Clear();
                        TeethField.Enabled = false;
                        TailField.Clear();
                        TailField.Enabled = false;
                        BreedField.Clear();
                        BreedField.Enabled = false;
                        LengthField.Enabled = true;
                        listAnimals.Items.Add(ReptileSpecies.Snake);
                        listAnimals.Items.Add(ReptileSpecies.Frog);
                        break;
                    }
            }

        }
        /// <summary>
        /// Updates the GUI depending on which animal is selected in the GUI.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void selectAnimal(object sender, EventArgs e)
        {
            if ((CategoryType)Enum.Parse(typeof(CategoryType), listCategory.SelectedItem.ToString()) == CategoryType.Mammal)
            {
                MammalSpecies animal = (MammalSpecies)Enum.Parse(typeof(MammalSpecies), listAnimals.SelectedItem.ToString());
                BreedField.Enabled = animal == MammalSpecies.Dog;
                TailField.Enabled = animal == MammalSpecies.Wolf;
            }
            else if ((CategoryType)Enum.Parse(typeof(CategoryType), listCategory.SelectedItem.ToString()) == CategoryType.Reptile)
            {
                ReptileSpecies animal = (ReptileSpecies)Enum.Parse(typeof(ReptileSpecies), listAnimals.SelectedItem.ToString());
                LengthField.Enabled = animal == ReptileSpecies.Snake;
            }
        }

    /// <summary>
    /// Deletes the selected animal in the list of all animals and updates the GUI.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Delete_Click(object sender, EventArgs e)
    {
        int index = 0;
        if (listView1.SelectedItems.Count != 0)
        {
            index = listView1.SelectedIndices[0];
            m_manager.DeleteAt(index);
            eaterBox.Clear();
            feedingBox.Clear();
            updateGUI();
        }
        else
        {
            return;
        }
    }

        /// <summary>
        /// Shows a form for adding a recipe when clicking on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addFoodButton_Click(object sender, EventArgs e)
        {
            RecipeForm recipeForm = new RecipeForm();
            Recipe r = new Recipe();
            recipeForm.isStaff = false;
            recipeForm.Recipe = r;
            recipeForm.ShowDialog();
            if (recipeForm.DialogResult == DialogResult.OK)
            {
                r = recipeForm.Recipe;
                m_foodManager.Add(r);
            }
            updateGUI();
        }


        /// <summary>
        /// Shows a form for adding a staff member when clicking on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addStaffButton_Click(object sender, EventArgs e)
        {
            RecipeForm recipeForm = new RecipeForm();
            Staff s = new Staff();
            recipeForm.isStaff = true;
            recipeForm.Staff = s;
            recipeForm.ShowDialog();
            if (recipeForm.DialogResult == DialogResult.OK)
            {
                s = recipeForm.Staff;
                m_staffManager.Add(s);
            }
            updateGUI();
        }

        /// <summary>
        /// Delets the selected item in the list of food and staff.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteButton_Click(object sender, EventArgs e)
        {
            object item = foodList.SelectedItem;

            if (item != null)
            {
                if (item.GetType() == typeof(Recipe))
                {
                    int index = foodList.SelectedIndex;
                    m_foodManager.DeleteAt(index);
                }
                else
                {
                    int index = foodList.SelectedIndex - m_foodManager.Count;
                    m_staffManager.DeleteAt(index);
                }
            }
            updateGUI();
        }

        /// <summary>
        /// Opens a form for changing the selected item in the list of food and staff members.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void foodList_DoubleClick_1(object sender, EventArgs e)
        {
            RecipeForm recipeForm = new RecipeForm();
            object item = foodList.SelectedItem;

            if (item != null)
            {
                if (item.GetType() == typeof(Recipe))
                {
                    int index = foodList.SelectedIndex;
                    recipeForm.Recipe = (Recipe)item;
                    recipeForm.isStaff = false;
                    recipeForm.ShowDialog();
                    if (recipeForm.DialogResult == DialogResult.OK)
                    {

                        m_foodManager.ChangeAt(recipeForm.Recipe, index);
                    }
                }
                else
                {
                    int index = foodList.SelectedIndex - m_foodManager.Count;
                    recipeForm.Staff = (Staff)item;
                    recipeForm.isStaff = true;
                    recipeForm.ShowDialog();
                    if (recipeForm.DialogResult == DialogResult.OK)
                    {
                        m_staffManager.ChangeAt(recipeForm.Staff, index);
                    }
                }
            }
            updateGUI();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Tries to open a new instance of the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFileNew_Click(object sender, EventArgs e)
        {
            if (!hasSaved)
            {
	            if (MessageBox.Show("Are you sure you want to continue without saving?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
	            {
	                this.Controls.Clear();
	                InitializeComponent();
	                Form1_Load(sender, e);
	            }
            }else{
			    this.Controls.Clear();
			    InitializeComponent();
			    Form1_Load(sender, e);
            }
        }

        /// <summary>
        ///Opens a file that the user selects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFileOpen_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = "../" + Application.StartupPath;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Filter = "Binary File (*.bin)|*.bin";
                openFileDialog1.FilterIndex = 2;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    m_manager.BinaryDeSerialize(openFileDialog1.FileName);
                    fileName = openFileDialog1.FileName;
                    updateGUI();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            hasSaved = false;
            updateGUI();
        }
        
        /// <summary>
        /// Saves the current instance to a file. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFileSaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.InitialDirectory = "../" + Application.StartupPath;
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.Filter = "Binary File (*.bin)|*.bin";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = saveFileDialog1.FileName;
                    m_manager.BinarySerialize(fileName);
                    updateGUI();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            hasSaved = true;
        }

        /// <summary>
        /// Saves the current instance to an existing file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFileSave_Click(object sender, EventArgs e)
        {
            try
            {
            if (fileName.Length > 0)
                {
                    m_manager.BinarySerialize(fileName);
                    updateGUI();
                }
                else
                {
                    mnuFileSaveAs_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            hasSaved = true;
        }

        /// <summary>
        /// Exports the recipe info to an XML file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFileXMLExport_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.InitialDirectory = "../" + Application.StartupPath;
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.Filter = "XML File (*.xml)|*.xml";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = saveFileDialog1.FileName;
                    m_foodManager.XMLSerialize(fileName);
                    updateGUI();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        /// <summary>
        /// Quits the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuFileExit_Click(object sender, EventArgs e)
        {
            
			if (!hasSaved)
			{
				if (MessageBox.Show("Are you sure you want to continue without saving?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					Application.Exit();
				}
			}
			else
			{
				Application.Exit();
			}
        }

    private void CategoryColumn_Click(object sender, EventArgs e)
    {
            }

    /// <summary>
    /// Is called when an index is selected in listView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void listView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Animal animal = null;
        if(listView1.SelectedItems.Count != 0) {
            int index = listView1.SelectedIndices[0];
            animal = m_manager.GetAt(index);
        }
        else
        {
            return;
        }
        eaterBox.Text = animal.GetEaterType().ToString();
        FoodSchedule schedule = animal.GetFoodSchedule();
        if (schedule != null)
        {
            if (schedule.Count > 0)
            {
                feedingBox.Text = schedule.ToString();
            }
            else
            {
                feedingBox.Text = schedule.DescribeNoFeedingRequired();
            }
        }
    }
}
