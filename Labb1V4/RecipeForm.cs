﻿//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Labb1
{
    /// <summary>
    /// A form for recipes and staff members. 
    /// </summary>
    public partial class RecipeForm : Form
    {

        private Recipe recipe;
        private Staff staff;
        private bool isstaff;

        /// <summary>
        /// Initializes the form.
        /// </summary>
        public RecipeForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the recipe for this form.
        /// </summary>
        public Recipe Recipe
        {
            get{ return recipe; }
            set { recipe = value; }
        }

        /// <summary>
        /// Returns true if the form should be for staff
        /// members or false if the form should be for recipes. 
        /// </summary>
        public bool isStaff
        {
            get { return isstaff; }
            set { isstaff = value; }
        }

        /// <summary>
        /// Gets or sets the staff member for this form.
        /// </summary>
        public Staff Staff {
            get { return staff;  }
            set { staff = value; }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void nameBox_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void ingredientsBox_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Adds an ingredient or a qualification to a recipe or staff member.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButton_Click(object sender, EventArgs e)
        {
            string value = ingredientsBox.Text;

            if (value.Equals(""))
            {
                System.Windows.Forms.MessageBox.Show("Must enter an ingredient/qualification");
                return;
            }

            if (!isStaff)
            {
                recipe.Ingredients.Add(value);    
            }
            else
            {
                staff.Qualifications.Add(value);
            }

            updateGUI();
        }

        /// <summary>
        /// Updates the GUI, specifically the list of ingredients or qualifications.
        /// </summary>
        private void updateGUI()
        {
            ingredientsList.Items.Clear();
            if (!isStaff)
            {
                ingredientsList.Items.AddRange(recipe.Ingredients.ToStringArray());
            }
            else
            {
                ingredientsList.Items.AddRange(staff.Qualifications.ToStringArray());
            }
			
            ingredientsBox.Clear();
        }

        /// <summary>
        /// Closes the form and sens a DialogResult.OK to the main form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okButton_Click(object sender, EventArgs e)
        {
            string name = nameBox.Text;
            if (name.Equals(""))
            {
                System.Windows.Forms.MessageBox.Show("Must enter a name, recipe/staff was not added");
                return;
            }
            if (!isStaff)
            {
                recipe.Name = name;
            }else
            {
                staff.Name = name;
            }
            
			this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// Loads the form and decides if the form is for staff or recipes. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecipeForm_Load(object sender, EventArgs e)
        {
            if (!isStaff)
            {
                nameBox.Text = recipe.Name;
            }else
            {
                groupBox1.Text = "Qualifications";
                ingredientsLabel.Text = "Qualification";
                nameBox.Text = staff.Name;
            }
           
            updateGUI();
        }

        private void ingredientsList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Changes the selected ingredient or qualification with the 
        /// input text in the Ingredient/Qualification text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void changeButton_Click(object sender, EventArgs e)
        {
            string value = ingredientsBox.Text;
            if (value.Equals(""))
            {
                System.Windows.Forms.MessageBox.Show("Must enter an ingredient/qualifcation");
                return;
            }
            int index = ingredientsList.SelectedIndex;
            if(index == -1)
            {
                System.Windows.Forms.MessageBox.Show("Must select an ingredient/qualification to change");
                return;
            }
            if (!isStaff)
            {
                recipe.Ingredients.ChangeAt(value, index);
            }else
            {
                staff.Qualifications.ChangeAt(value, index);
            }
            
            updateGUI();
        }

        /// <summary>
        /// Delets the selected qualification or ingredient. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteButton_Click(object sender, EventArgs e)
        {
            int index = ingredientsList.SelectedIndex;
            if (index == -1)
            {
                System.Windows.Forms.MessageBox.Show("Must select an ingredient to delete");
                return;
            }
            if (!isStaff)
            {
                recipe.Ingredients.DeleteAt(index);
            }
            else
            {
                staff.Qualifications.DeleteAt(index);
            }
            updateGUI();
        }
    }
}
