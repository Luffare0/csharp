//Author: Gustav Svensson
//Date: 8/5 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using Labb1;
/// <summary>
/// The wolf class. Represents a wolf.
/// Inherits from Mammal class. 
/// </summary>
[Serializable]
public class Wolf : Mammal {
    private int tailLength;
    private FoodSchedule foodschedule;
    private EaterType eatertype;


	/// <summary>
	/// Initializes a new instance of the <see cref="T:Wolf"/> class.
	/// </summary>
    public Wolf()
    {
        eatertype = EaterType.Carnivore;
        foodschedule = new FoodSchedule();
        foodschedule.AddFoodScheduleItem("Morning: Meat");
        foodschedule.AddFoodScheduleItem("Lunch: Meat");
        foodschedule.AddFoodScheduleItem("Evening: Meat");
        spec = "Length of tail is " + tailLength + ". ";
    }
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Wolf"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="name">Name.</param>
	/// <param name="age">Age.</param>
	/// <param name="gender">Gender.</param>
	/// <param name="category">Category.</param>
	/// <param name="teeth">Teeth.</param>
	/// <param name="tailLength">Tail length.</param>
    public Wolf(int id, string name, int age, GenderType gender, CategoryType category, int teeth, int tailLength) : base(id, name, age, gender, category, teeth){
        this.tailLength = tailLength;
        eatertype = EaterType.Carnivore;
        foodschedule = new FoodSchedule();
        foodschedule.AddFoodScheduleItem("Morning: Meat");
        foodschedule.AddFoodScheduleItem("Lunch: Meat");
        foodschedule.AddFoodScheduleItem("3PM: Meat");
        spec = "Length of tail is " + tailLength + ". ";
    }
	/// <summary>
	/// Gets or sets the length of the tail.
	/// </summary>
	/// <value>The length of the tail.</value>
    public int TailLength {
        get { return tailLength; }
        set { tailLength = value; }
    }
	/// <summary>
	/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Wolf"/>.
	/// </summary>
	/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Wolf"/>.</returns>
    public override string ToString()
    {
        return base.ToString();
    }

    public override string GetSpecies()
    {
        return "wolf";
    }

    public override FoodSchedule GetFoodSchedule()
    {
        return foodschedule;
    }

    public override EaterType GetEaterType()
    {
        return eatertype;
    }
}
