﻿//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Labb1;

    [Serializable]
    public class ListManager<T> : IListManager<T>
    {
        private List<T> m_list;

        public ListManager()
        {
            m_list = new List<T>();
        }

        public int Count
        {
            get
            {
                return m_list.Count;
            }
        }


        public virtual bool Add(T aType)
        {
            m_list.Add(aType);
            return true;
        }

        public bool ChangeAt(T aType, int anindex)
        {
            m_list[anindex] = aType;
            return true;
        }

        public bool CheckIndex(int index)
        {
            return index < m_list.Count;
        }

        public void DeleteAll()
        {
            m_list.Clear();
        }

        public bool DeleteAt(int anIndex)
        {
            m_list.RemoveAt(anIndex);
            return true;
        }

		public T GetAt(int anIndex)
        {
            return m_list[anIndex];
        }

        public string[] ToStringArray()
        {
            string[] stringarray = new string[m_list.Count];
            for (int i = 0; i < m_list.Count; i++){
                stringarray[i] = m_list[i].ToString();
            }
            return stringarray;
        }

        public List<string> ToStringList()
        {
            List<String> stringlist = new List<String>();
            for (int i = 0; i < m_list.Count; i++)
            {
                stringlist[i] = m_list[i].ToString();
            }
            return stringlist;
        }

        public bool BinaryDeSerialize(string fileName)
        {
            m_list = BinSerializerUtility.Deserialize<List<T>>(fileName);
            return true;
        }

        public bool BinarySerialize(string fileName)
        {
            return BinSerializerUtility.BinSerialize(m_list, fileName);
        }

        public bool XMLSerialize(string fileName)
        {
            return XMLSerializerUtility.SerializeToFile<List<T>>(fileName, m_list);
        }
    }

