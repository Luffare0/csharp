﻿//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Labb1;

    /// <summary>
    /// Interface for implementation by manmager classes hosting a collection of the type 
    /// List<T> where T can be any object type. In this documentation, 
    /// the collection is referred to as m_list. IListManager can be implemented by 
    /// different classes passing any type <T> at declaration but then T must have the same type in all methods including in this 
    /// interface.
    /// </summary>
    /// <typeparam name="T">object type</typeparam>
    interface IListManager<T>
    {
		/// <summary>
		/// Add an object to the collection m_list. 
		/// </summary>
		/// <param name="aType">A type</param>
		/// <returns>True if successful, false otherwise</returns>
		bool Add(T aType);

        /// <summary>
        /// Change an object in the collection m_list at the given index.
        /// </summary>
        /// <param name="aType">A type</param>
        /// <param name="anindex">An index</param>
        /// <returns>True if successful, false otherwise</returns>
        bool ChangeAt(T aType, int anindex);

        /// <summary>
        /// Check if the index is within the boundaries of the collection m_list.
        /// </summary>
        /// <param name="index"></param>
        /// <returns>True if successful, false otherwise</returns>
        bool CheckIndex(int index);

        /// <summary>
        /// Deletes all of the items in the collection m_list.
        /// </summary>
        void DeleteAll();

        /// <summary>
        /// Remove an object from the collection m_list at
        /// a given position. 
        /// </summary>
        /// <param name="anIndex">Index to the object that is to be removed.</param>
        /// <returns>True if successful, false otherwise.</returns>
        bool DeleteAt(int anIndex);

        /// <summary>
        /// Gets the element T in the collection m_list at the given index.
        /// </summary>
        /// <param name="anIndex">An index.</param>
        /// <returns></returns>
        T GetAt(int anIndex);

        /// <summary>
        /// Returns an array of the string representations of 
        /// the items in the m_list collection.
        /// </summary>
        /// <returns>A string array</returns>
        string[] ToStringArray();

        /// <summary>
        /// Returns a list of the string representations of 
        /// the items in the m_list collection.
        /// </summary>
        /// <returns>A string list.</returns>
        List<string> ToStringList();

        /// <summary>
        /// Returns the number of items in the m_list collection.
        /// </summary>
        int Count { get; }

        bool BinarySerialize(string fileName);

        bool BinaryDeSerialize(string fileName);

        bool XMLSerialize(string fileName);
    }

