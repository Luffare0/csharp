//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using Labb1;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

/// <summary>
/// The Animal manager class. This class uses most of the methods of the ListManager class directly.
/// However, before adding a new object to the collection, it generates an ID and attaches to the object.
/// ListManager class. 
/// </summary>
[Serializable]
public class AnimalManager  : ListManager<Animal>{
  	private int currentId;

	/// <summary>
	/// Initializes a new instance of the <see cref="T:AnimalManager"/> class. Initilizes the m_animalList. 
	/// which holds all animals. 
	/// </summary>
 	public AnimalManager(){
        currentId = 0;  
  	}
	/// <summary>
	/// Adds the animal to the m_animalList. 
	/// </summary>
	/// <param name="animalObj">Animal object.</param>
    public override bool Add(Animal animalObj)
    {
		animalObj.Id = currentId++;
        return base.Add(animalObj);
    }
}
