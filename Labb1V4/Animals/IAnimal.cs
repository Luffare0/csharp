﻿//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using Labb1;
using System;
public interface IAnimal
{
    /// <summary>
	/// Gets or sets the name.
	/// </summary>
	/// <value>The name.</value>
    string Name
    {
        get;
        set;
    }

    /// <summary>
	/// Gets or sets the id.
	/// </summary>
	/// <value>The identifier.</value>
    int Id
    {
        get; 
        set;
    }

    /// <summary>
	/// Gets or sets the gender.
	/// </summary>
	/// <value>The gender.</value>
    GenderType Gender
    {
        get; 
        set;   
    }

    /// <summary>
    /// Gets the eatertype of a animal.
    /// </summary>
    /// <returns></returns>
    EaterType GetEaterType();

    /// <summary>
    /// Gets the species of a animal.
    /// </summary>
    /// <returns></returns>
    string GetSpecies();


    /// <summary>
    /// gets the foodschedule of a animal
    /// </summary>
    /// <returns></returns>
    FoodSchedule GetFoodSchedule();
}