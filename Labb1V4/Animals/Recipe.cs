﻿//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Labb1;

    [Serializable]
    public class Recipe
    {

        private string name;
        private ListManager<string> m_ingredients;
       

        public Recipe()
        {
            m_ingredients = new ListManager<string>();
        }

        public ListManager<string> Ingredients{
            get { return m_ingredients; }
            set { m_ingredients = value; }
        }

		public int Count { 
			get { return m_ingredients.Count; }
		}

        public string Name {
            get { return name; }
            set { name = value; }
        }

        public override string ToString()
        {
            string recipe =  "";
            int size = m_ingredients.Count;
			recipe += name + ", ";
            for(int i = 0; i  < size; i++)
            {
				if (i == size - 1)
				{
					recipe += m_ingredients.GetAt(i);
				}
				else
				{
					recipe += m_ingredients.GetAt(i) + ", ";
				}
					
            }
            return recipe;
        }
    }

