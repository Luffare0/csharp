//Author: Gustav Svensson
//Date: 19/4 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
/// <summary>
/// The Reptile class. Represents a reptile.
/// Inherits from Animal class.
/// </summary>
[Serializable]
public abstract class Reptile : Animal {
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Reptile"/> class.
	/// </summary>
    public Reptile()
    {

    }
	/// <summary>
	/// Initializes a new instance of the <see cref="T:Reptile"/> class.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="name">Name.</param>
	/// <param name="age">Age.</param>
	/// <param name="gender">Gender.</param>
	/// <param name="category">Category.</param>
    public Reptile(int id, string name, int age, GenderType gender, CategoryType category) : base(id, name, age, gender, category){
    
  }
	/// <summary>
	/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Reptile"/>.
	/// </summary>
	/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Reptile"/>.</returns>
    public override string ToString()
    {
        return base.ToString();
    }


}
