﻿//Author: Gustav Svensson
//Date: 8/5 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Labb1;

    /// <summary>
    /// A tool for serializing object to XML
    /// </summary>
    class XMLSerializerUtility
    {

        /// <summary>
        /// XML serializes a object to a file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool SerializeToFile<T>(string filePath, T obj)
        {
            bool success = true; 
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            FileStream writer = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            try
            {
                serializer.Serialize(writer, obj);
            }
            catch
            {
                success = false;
                throw;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
            return success;
        }
    }
