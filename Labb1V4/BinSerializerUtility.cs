﻿//Author: Gustav Svensson
//Date: 8/5 - 2017
//Project: Lab1 in second C# course Malmö Univeristy
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using Labb1;

    /// <summary>
    /// A tool for binary serialization for objects.
    /// </summary>
    class BinSerializerUtility
    {

        /// <summary>
        /// Serializes an object to a binary file.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool BinSerialize(object obj, string filePath)
        {
            FileStream file = null;
            bool success = true;
            try
            {
                file = new FileStream(filePath, FileMode.Create);
                file.Position = 0;
                BinaryFormatter binFormatter = new BinaryFormatter();
                binFormatter.Serialize(file, obj);
            }
            //catch
            //{
            //    success = false;
            //    throw;
            //}
            finally
            {
                if (file != null)
                    file.Close();
            }
            return success;
        }

        /// <summary>
        /// Deserializes an object from a binary file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string filePath)
        {
            FileStream file = null;
            object obj = null;

            try
            {
                if(!File.Exists(filePath))
                {
                    throw new FileNotFoundException("The file is not found. ", filePath);
                }
                file = new FileStream(filePath, FileMode.Open);
                file.Position = 0;

                BinaryFormatter binFormatter = new BinaryFormatter();
                obj = binFormatter.Deserialize(file);
            }
            finally
            {
                if(file != null)
                {
                    file.Close();
                }
            }
            return (T)obj;
        }
    }

